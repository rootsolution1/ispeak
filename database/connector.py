import MySQLdb
from datetime import datetime
from containers.user import User
from containers.deepuser import DeepUser
from containers.progress import Progress
import uuid


def generateID():
    return uuid.uuid4()


class Connector:
    def __init__(self):
        self.__host = "localhost"
        self.__username = "root"
        self.__password = "initial"
        self.__dbname = "iSpeak"
        self.__db = MySQLdb.connect(self.__host, self.__username, self.__password, self.__dbname)
        self.__cursor = self.__db.cursor()

    """
    General user actions
    """

    def getAllUsers(self):
        users = []
        self.__cursor.execute("SELECT * FROM users_data")
        results = self.__cursor.fetchall()
        for res in results:
            users.append(User(res[1], res[2]))
        return users

    def checkUser(self, user):
        self.__cursor.execute("SELECT * FROM users_data where login =%s  and paswd=%s",
                              (user.getUserName(), user.getPassword()))
        return self.__cursor.rowcount > 0

    def getUserID(self, user):
        self.__cursor.execute("SELECT * FROM users_data where login =%s  and paswd=%s",
                              (user.getUserName(), user.getPassword()))
        return self.__cursor.fetchone()[0]

    def postUser(self, user):
        timestamp = datetime.now().isoformat()
        self.__cursor.execute("INSERT INTO  users_data (id,login,paswd,last_action,is_active) VALUES ( %s,%s,%s,%s,%s)",
                              (str(generateID()), user.getUserName(), user.getPassword(), timestamp, 0))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def isActive(self,user):
        self.__cursor.execute("SELECT is_active FROM users_data where login =%s  and paswd=%s",
                              (user.getUserName(), user.getPassword()))
        return self.__cursor.fetchone()

    def deleteUser(self, user):
        pass

    def alterUser(self, user_id, new_user):
        self.__cursor.execute("UPDATE users_data SET paswd = %s where id = %s", (new_user.getPassword(), user_id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def setUserActive(self, user):
        self.__cursor.execute("UPDATE users_data SET is_active = %s where id = %s", (user.is_active, user.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    """
    Deep user actions
    """
    def setUserMetadata(self,deep_user):
        self.__cursor.execute("INSERT INTO  user_metadata (id,display_name, level ,language, picture) VALUES ( %s,%s,%s,%s,%s)",
                              (deep_user.id, deep_user.display_name, deep_user.level, deep_user.language, deep_user.profile_picture))
        self.__db.commit()
        return self.__cursor.rowcount > 0
    def getUserMetadata(self, uid):
        self.__cursor.execute("SELECT * FROM user_metadata where id =%s",
                              [uid])
        d = self.__cursor.fetchone()
        return DeepUser(d[0], d[1], d[2], d[3], d[4])

    def changeDeepUserName(self, new_deep_user):
        self.__cursor.execute("UPDATE user_metadata SET display_name = %s where id = %s",
                              (new_deep_user.display_name, new_deep_user.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def changeDeepUserLanguage(self, new_deep_user):
        self.__cursor.execute("UPDATE user_metadata SET language = %s where id = %s",
                              (new_deep_user.language, new_deep_user.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def changeUserLevel(self, new_deep_user):
        self.__cursor.execute("UPDATE user_metadata SET level = %s where id = %s",
                              (new_deep_user.level, new_deep_user.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def changeUserPicture(self, new_deep_user):
        self.__cursor.execute("UPDATE user_metadata SET picture = %s where id = %s",
                              (new_deep_user.picture, new_deep_user.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def postUserMetadata(self, deep_user):
        self.__cursor.execute(
            "INSERT INTO  user_metadata (id,display_name,level,language,picture) VALUES ( %s,%s,%s,%s,%s)",
            (deep_user.id, deep_user.display_name, deep_user.level, deep_user.language, deep_user.profile_picture))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def addContact(self, contact):
        pass

    """
    Progress requests
    """
    def getProgress(self, uid):
        self.__cursor.execute("SELECT * FROM user_progress where id =%s",
                              [uid])
        d = self.__cursor.fetchone()
        return Progress(uid, d[1], d[2], d[3])

    def setProgress(self,progress):
        self.__cursor.execute(
            "INSERT INTO  user_progress (id,study_genre,book,raw_id) VALUES ( %s,%s,%s,%s)",
            (progress.id, progress.study_genre, progress.book, progress.raw))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def changeGenre(self, progress):
        self.__cursor.execute("UPDATE user_progress SET study_genre = %s where id = %s",
                              (progress.study_genre, progress.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0

    def changeBook(self, progress):
        self.__cursor.execute("UPDATE user_progress SET book = %s where id = %s",
                              (progress.book, progress.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0


    def changeRaw(self, progress):
        self.__cursor.execute("UPDATE user_progress SET raw_id = %s where id = %s",
                              (progress.raw, progress.id))
        self.__db.commit()
        return self.__cursor.rowcount > 0


    def getBook(self,book_id):
        pass

    def getRaw(self,raw_id,book):
        pass
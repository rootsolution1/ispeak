from flask import Flask
from flask_cors import CORS

from routes.login import login_routes
from routes.account import account_routes
from routes.study import study_routes
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.register_blueprint(login_routes, url_prefix='/auth')
app.register_blueprint(account_routes, url_prefix='/account')
app.register_blueprint(study_routes,url_prefix="/study")
class User:
    def __init__(self, username, password):
        self.id = None
        self.username = username
        self.password = password
        self.is_active = None
    
    def getUserName(self):
        return self.username
    
    def getPassword(self):
        return self.password

    def setID(self, uid):
        self.id = uid

    def getID(self):
        return self.id
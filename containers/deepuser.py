class DeepUser:
    def __init__(self, uid, display_name, level ,language, profile_picture):
        self.id = uid
        self.display_name = display_name
        self.level = level
        self.language = language
        self.profile_picture = profile_picture

from flask import Blueprint, make_response, jsonify, request
from containers.deepuser import DeepUser
from containers.user import User
from database.connector import Connector
from basicauth import decode

login_routes = Blueprint("login", __name__)
connector = Connector()


@login_routes.route("/login", methods=['GET'])
def login():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        u.setID(connector.getUserID(u))
        u.is_active = True
        changes = connector.setUserActive(u)
        if changes:
            return make_response("Success",200)
        else:
            return make_response("User is already online", 401)
    else:
        return make_response("Wrong credentials", 401)


@login_routes.route("/logout", methods=['GET'])
def logout():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        u.setID(connector.getUserID(u))
        u.is_active = False
        changes = connector.setUserActive(u)
        if changes:
            return make_response("Success", 200)
        else:
            return make_response("User is already offline", 401)
    else:
        return make_response("Action is not allowed", 401)


@login_routes.route("/stayalive", methods=['GET'])
def stayalive():
    return "I am alive"


@login_routes.route("/test", methods=['GET'])
def test_route():
    d = DeepUser("random3", "Stepan5", 3, 5, "some/path.jpg")
    return make_response(
        jsonify({"Hello,there": 1324, "users": connector.postUserMetadata(d)}), 200)

from flask import Blueprint, make_response, jsonify, request
from containers.deepuser import DeepUser
from containers.progress import Progress
from containers.user import User
from database.connector import Connector
from basicauth import decode

study_routes = Blueprint("study", __name__)
connector = Connector()


@study_routes.route("/data", methods=['GET'])
def getStudyMetadata():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = connector.getProgress(uid)
            return make_response(jsonify(meta.__dict__), 200)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


@study_routes.route("/data", methods=['POST'])
def setStudyMetadata():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = Progress(uid, request.values.get("study_genre"), request.values.get("book"),
                            request.values.get("raw"))
            if connector.setProgress(meta):
                return make_response(meta.__dict__, 200)
            else:
                return make_response("Bad parameters", 401)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


@study_routes.route("/change", methods=['POST'])
def changeStudyData():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = Progress(uid, request.values.get("study_genre"), request.values.get("book"),
                            request.values.get("raw"))
            old_meta = connector.getProgress(uid)
            if (old_meta.study_genre != meta.study_genre):
                connector.changeGenre(meta)
            if (old_meta.book != meta.book):
                connector.changeBook(meta)
            if (old_meta.raw != meta.raw):
                connector.changeRaw(meta)
            return make_response(meta.__dict__, 200)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


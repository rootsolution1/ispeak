from flask import Blueprint, make_response, jsonify, request
from containers.deepuser import DeepUser
from containers.user import User
from database.connector import Connector
from basicauth import decode

account_routes = Blueprint("account", __name__)
connector = Connector()


@account_routes.route("/data", methods=['GET'])
def getAccountData():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = connector.getUserMetadata(uid)
            return make_response(jsonify(meta.__dict__), 200)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


@account_routes.route("/data", methods=['POST'])
def setAccountData():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = DeepUser(uid, request.values.get("display_name"), request.values.get("level"),
                            request.values.get("language"), request.values.get("picture"))
            if connector.setUserMetadata(meta):
                return make_response(meta.__dict__, 200)
            else:
                return make_response("Bad parameters", 401)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


@account_routes.route("/change", methods=['POST'])
def changeAccountData():
    auth = request.headers.get('Authorization')
    user_cred = decode(auth)
    u = User(user_cred[0], user_cred[1])
    user_exists = connector.checkUser(u)
    if user_exists:
        uid = connector.getUserID(u)
        if connector.isActive(u):
            meta = DeepUser(uid, request.values.get("display_name"), request.values.get("level"),
                            request.values.get("language"), request.values.get("picture"))
            old_meta = connector.getUserMetadata(uid)
            if (old_meta.display_name != meta.display_name):
                connector.changeDeepUserName(meta)
            if (old_meta.level != meta.level):
                connector.changeUserLevel(meta)
            if (old_meta.language != meta.language):
                connector.changeDeepUserLanguage(meta)
            if (old_meta.profile_picture != meta.profile_picture):
                connector.changeUserPicture(meta)
            return make_response(meta.__dict__, 200)
        else:
            return make_response("Action is not allowed", 403)
    else:
        return make_response("User don't exists", 404)


